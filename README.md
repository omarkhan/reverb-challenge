Reverb Challenge
================

This is my attempt at [Reverb's technical challenge](https://reverb.com/page/dev-challenge).

### Design choices

Uses sqlite for data storage. Fits the requirement of storing data in a file,
allows SQL queries, indexing, rock-solid library.

### Setup

Run `bundle install`.

### Testing

Run `rake test`.

### Running

Run `rackup`.
