require 'minitest/autorun'
require_relative '../lib/database'
require_relative '../lib/record'
require_relative 'fixtures'

class DatabaseTest < Minitest::Test
  include Fixtures

  def setup
    @database = Database.new(':memory:')
    records.values.each { |record| @database.insert_record(record) }
  end

  def test_sort_by_favorite_color
    assert_equal records.values_at(:omar, :buddy, :ed), @database.sort_by_favorite_color
  end

  def test_sort_by_date_of_birth
    assert_equal records.values_at(:ed, :buddy, :omar), @database.sort_by_date_of_birth
  end

  def test_sort_by_last_name
    assert_equal records.values_at(:ed, :omar, :buddy), @database.sort_by_last_name
  end
end
