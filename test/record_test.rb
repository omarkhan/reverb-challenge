require 'minitest/autorun'
require_relative '../lib/record'

class RecordTest < Minitest::Test
  def test_missing_fields
    assert_raises(Record::MissingFields) { Record.new('Guy', 'Buddy') }
  end

  def test_too_many_fields
    assert_raises(Record::TooManyFields) { Record.new('Guy', 'Buddy', 'Blue', '1936-07-30', '✌') }
  end

  def test_invalid_date_of_birth
    assert_raises(Record::InvalidDate) { Record.new('Guy', 'Buddy', 'Blue', '1936-30-07') }
  end

  def test_invalid_date_of_birth_format
    assert_raises(Record::InvalidDate) { Record.new('Guy', 'Buddy', 'Blue', '30/07/1936') }
  end
end
