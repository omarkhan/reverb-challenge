require 'minitest/autorun'
require_relative '../lib/parser'
require_relative '../lib/record'
require_relative 'fixtures'

class ParserTest < Minitest::Test
  include Fixtures

  def test_pipe_separator
    parser = Parser.new(separator: '|')
    input = records.values.map { |record| record.to_a.join(' | ') }.join("\n")
    assert_equal records.values, parser.parse(input)
  end

  def test_comma_separator
    parser = Parser.new(separator: ',')
    input = records.values.map { |record| record.to_a.join(', ') }.join("\n")
    assert_equal records.values, parser.parse(input)
  end

  def test_missing_separator
    parser = Parser.new(separator: '|')
    input = records.values.map { |record| record.to_a.join("\t") }.join("\n")
    assert_raises(Parser::MissingSeparator) { parser.parse(input) }
  end

  def test_missing_fields
    parser = Parser.new(separator: '|')
    input = records.values.map { |record| record.to_a[0..1].join(' | ') }.join("\n")
    assert_raises(Record::MissingFields) { parser.parse(input) }
  end

  def test_too_many_fields
    parser = Parser.new(separator: '|')
    input = records.values.map { |record| record.to_a.push('✌').join(' | ') }.join("\n")
    assert_raises(Record::TooManyFields) { parser.parse(input) }
  end
end
