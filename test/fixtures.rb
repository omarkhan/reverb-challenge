module Fixtures
  def records
    @records ||= {
      buddy: Record.new('Guy', 'Buddy', 'Blue', '1936-07-30'),
      ed:    Record.new("O'Hare", 'Edward "Butch"', 'Blue', '1914-03-13'),
      omar:  Record.new('Khan', 'Omar', 'Red', '1986-07-17')
    }
  end
end
