require 'json'
require 'minitest/around/unit'
require 'minitest/autorun'
require 'rack/test'
require_relative '../api'
require_relative 'fixtures'

class APITest < Minitest::Test
  include Rack::Test::Methods
  include Fixtures

  def app
    API
  end

  def around
    database = Database.new(':memory:')
    records.values.each { |record| database.insert_record(record) }
    API.stub(:database, database) { yield }
  end

  def test_create_record_with_pipe_separator
    post '/records', 'Waters | Muddy | Blue | 1913-04-04'
    assert_equal 201, last_response.status
    assert_equal 1, response_json[:count]
  end

  def test_create_record_with_comma_separator
    post '/records', 'Waters, Muddy, Blue, 1913-04-04'
    assert_equal 201, last_response.status
    assert_equal 1, response_json[:count]
  end

  def test_create_record_no_input
    post '/records'
    assert_equal 400, last_response.status
    assert_equal 'NoRecordsGiven', response_json[:error]
  end

  def test_create_record_missing_fields
    post '/records', 'Muddy, Waters'
    assert_equal 400, last_response.status
    assert_equal 'MissingFields', response_json[:error]
  end

  def test_create_record_missing_separator
    post '/records', 'Muddy Waters'
    assert_equal 400, last_response.status
    assert_equal 'MissingSeparator', response_json[:error]
  end

  def test_list_records_by_birthdate
    get '/records/birthdate'
    assert_equal 200, last_response.status
    assert_equal records.values_at(:ed, :buddy, :omar).map(&:to_h), response_json[:records]
  end

  def test_list_records_by_name
    get '/records/name'
    assert_equal 200, last_response.status
    assert_equal records.values_at(:ed, :omar, :buddy).map(&:to_h), response_json[:records]
  end

  private

  def response_json
    JSON.parse(last_response.body, symbolize_names: true)
  end
end
