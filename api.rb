require 'grape'
require_relative 'lib/database'
require_relative 'lib/parser'
require_relative 'lib/record'

class API < Grape::API
  format :json

  resource :records do
    desc 'Add records.'
    post do
      records = parse_request_body
      raise NoRecordsGiven, 'You must send at least 1 record.' if records.empty?
      records.each { |record| API.database.insert_record(record) }
      { message: 'Records added.', count: records.length }
    end

    desc 'Return records sorted by date of birth (ascending).'
    get :birthdate do
      { records: API.database.sort_by_date_of_birth.map(&:to_h) }
    end

    desc 'Return records sorted by last name (descending).'
    get :name do
      { records: API.database.sort_by_last_name.map(&:to_h) }
    end
  end

  helpers do
    def parse_request_body
      input = request.body.read
      error = nil
      API.parsers.each do |parser|
        begin
          return parser.parse(input)
        rescue Parser::MissingSeparator => e
          error = e
        end
      end
      raise error unless error.nil?
      []
    end
  end

  def self.parsers
    @parsers ||= [Parser.new(separator: '|'), Parser.new(separator: ',')]
  end

  def self.database
    @database ||= Database.new('db.sqlite')
  end

  class NoRecordsGiven < StandardError
  end

  rescue_from NoRecordsGiven, Parser::MissingSeparator, Record::Error do |e|
    error!({ error: e.class.name.split('::').last, message: e.to_s }, 400)
  end
end
