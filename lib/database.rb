require 'sqlite3'
require_relative 'record'

class Database
  def initialize(path)
    @db = SQLite3::Database.new(path)
    create_records_table
    create_indexes
  end

  def sort_by_favorite_color
    select_records('ORDER BY favorite_color DESC, last_name ASC')
  end

  def sort_by_date_of_birth
    select_records('ORDER BY date_of_birth ASC')
  end

  def sort_by_last_name
    select_records('ORDER BY last_name DESC')
  end

  def insert_record(record)
    @db.execute('INSERT INTO records VALUES (?, ?, ?, ?, ?)', [nil] << record.to_a)
  end

  private

  def create_records_table
    @db.execute <<~SQL
      CREATE TABLE IF NOT EXISTS records (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        last_name TEXT NOT NULL,
        first_name TEXT NOT NULL,
        favorite_color TEXT NOT NULL,
        date_of_birth TEXT NOT NULL
      )
    SQL
  end

  def create_indexes
    @db.execute <<~SQL
      CREATE INDEX IF NOT EXISTS index_records_on_favorite_color_and_last_name ON records (
        favorite_color DESC,
        last_name ASC
      )
    SQL
    @db.execute <<~SQL
      CREATE INDEX IF NOT EXISTS index_records_on_date_of_birth ON records (
        date_of_birth ASC
      )
    SQL
    @db.execute <<~SQL
      CREATE INDEX IF NOT EXISTS index_records_on_last_name ON records (
        last_name DESC
      )
    SQL
  end

  def select_records(order = nil)
    @db.execute("SELECT * FROM records #{order}").map { |row| Record.new(*row[1..-1]) }
  end
end
