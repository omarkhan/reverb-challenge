require 'csv'
require_relative 'record'

class Parser
  def initialize(separator:)
    @separator = separator
  end

  def parse(input)
    CSV.parse(input, col_sep: @separator, row_sep: "\n", liberal_parsing: true).map do |row|
      raise MissingSeparator, row[0] if row.length == 1
      Record.new(*row.map(&:strip))
    end
  end

  class MissingSeparator < StandardError
  end
end
