require 'date'

class Record < Struct.new(:last_name, :first_name, :favorite_color, :date_of_birth)
  def initialize(*fields)
    raise MissingFields, fields if fields.length < members.length
    raise TooManyFields, fields if fields.length > members.length
    super
    raise InvalidDate, date_of_birth unless valid_date_of_birth?
  end

  class Error < StandardError
  end

  class MissingFields < Error
  end

  class TooManyFields < Error
  end

  class InvalidDate < Error
  end

  private

  def valid_date_of_birth?
    return false unless date_of_birth =~ /^\d{4}-\d{2}-\d{2}$/
    Date.valid_date?(*date_of_birth.split('-').map(&:to_i))
  end
end
